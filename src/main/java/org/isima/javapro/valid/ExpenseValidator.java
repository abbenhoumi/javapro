package org.isima.javapro.valid;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.codehaus.jackson.map.ObjectMapper;
import org.isima.javapro.model.Employe;
import org.isima.javapro.config.Parameter;
import org.isima.javapro.expense.Expense;
import org.isima.javapro.metier.CustomLog;
import org.isima.javapro.metier.ValidationRegle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class ExpenseValidator {

	private CustomLog log = new CustomLog();

	private List<Expense> invalidExpenses;
	private List<Expense> validExpenses ;
	private List<Employe> employees ; 
	
	public ExpenseValidator() {
		validExpenses = new ArrayList<>();
		invalidExpenses = new ArrayList<>();
		employees = new ArrayList<>();
	}
	
	

	private void configurationSystemProxy() {
		System.setProperty("http.proxyHost", Parameter.PROXYHOST);
		System.setProperty("http.proxyPort", Parameter.PROXYPORT);
		System.setProperty("https.proxyHost", Parameter.PROXYHOST);
		System.setProperty("https.proxyPort", Parameter.PROXYPORT);
	}

	private void lectureFileExcel(String toFile, List<Expense> validExpenses, List<Expense> invalidExpenses) {
		CSVFormat csvFormat = CSVFormat.DEFAULT
				.withHeader("id", "employee", "date", "category", "description", "amount").withDelimiter(';')
				.withTrim();

		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		try (BufferedReader reader = new BufferedReader(new FileReader(toFile));
				CSVParser csvParser = new CSVParser(reader, csvFormat);) {

			for (CSVRecord csvRec : csvParser) {
				if (!"id".equals(csvRec.get("id"))) {
					LocalDate date = LocalDate.parse(csvRec.get("date"), dateFormatter);
					double amount = Double.parseDouble(csvRec.get("amount"));

					Expense expense = new Expense(csvRec.get("id"), csvRec.get("employee"), date,
							csvRec.get("category"), csvRec.get("description"), amount);

					addEmploye(expense.getEmployee() , expense);
					
					if (ValidationRegle.isEmployeeExists(expense.getEmployee()) && expense.getAmount() > 0
							&& expense.getAmount() < Double.MAX_VALUE && ValidationRegle.havePolicyAmount(expense))
						validExpenses.add(expense);
					else
						invalidExpenses.add(expense);
				}
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}

	private void listToSave(String file, List<Expense> listToSave) {
		listToSave.forEach(expense -> {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				String jsonString = objectMapper.writeValueAsString(expense);
				File historyFile = new File(file);
				Path pathHistoryFile = historyFile.getParentFile().toPath();
				if (!pathHistoryFile.toFile().exists())
					Files.createDirectories(historyFile.getParentFile().toPath());
				if (historyFile.exists())
					Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
				else
					Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
				log.info("expense saved in : " + file + " file.");
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		});
	}

	public void validate() {

		configurationSystemProxy();

	
		String toFile = "/tmp/expenses.csv";

		try {
			FileUtils.copyURLToFile(new URL(Parameter.EXPENSE_FILE_URL), new File(toFile), 10000, 10000);
		} catch (IOException e) {
			log.error(e.getMessage());
			String localExpenseFile = getClass().getClassLoader().getResource("expenses.csv").getFile();
			try {
				FileUtils.copyFile(new File(localExpenseFile), new File(toFile));
			} catch (IOException ex) {
				log.error(ex.getMessage());
			}
		}

		// lecture du fichier excel
		lectureFileExcel(toFile, validExpenses, invalidExpenses);

		Map<String, List<Expense>> expensesByEmployee = validExpenses.stream()
				.collect(groupingBy(Expense::getEmployee));

		List<Expense> expensesToSave = new ArrayList<>();
		expensesByEmployee.forEach((k, v) -> {
			double limit = 1000;
			boolean repectMonthPolicy = validExpenses.stream()
					.collect(groupingBy(Expense::getMonth, summingDouble(Expense::getAmount))).entrySet().stream()
					.allMatch(entry -> entry.getValue() < limit);
			if (repectMonthPolicy)
				expensesToSave.addAll(v);
			else
				invalidExpenses.addAll(v);
		});

		listToSave(Parameter.HISTORY_FILE, expensesToSave);
		listToSave(Parameter.INVALID_HISTORY_FILE, invalidExpenses);

	}


	public void addEmploye(String employe, Expense expense) {
		int i = 0 ; 
		for(Employe emp : employees) {
			if(emp.getNom().equals(employe)) {
				List<Expense> newList = emp.getExpenses() ; 
				newList.add(expense);
				emp.setExpenses(newList);
			}
			i++ ; 
		}
		
		if(i == employees.size()) {
			List<Expense> newList = new ArrayList<>() ; 
			newList.add(expense);
			employees.add(new Employe(employe, newList)) ; 
		}
	}
	public List<Expense> getInvalidExpenses() {
		return invalidExpenses;
	}

	public void setInvalidExpenses(List<Expense> invalidExpenses) {
		this.invalidExpenses = invalidExpenses;
	}

	public List<Expense> getValidExpenses() {
		return validExpenses;
	}
	public void setValidExpenses(List<Expense> validExpenses) {
		this.validExpenses = validExpenses;
	}
	
	public List<Employe> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employe> employees) {
		this.employees = employees;
	}
}
