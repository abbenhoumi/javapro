package org.isima.javapro;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.isima.javapro.expense.Expense;
import org.isima.javapro.metier.ValidationRegle;
import org.isima.javapro.model.Employe;
import org.isima.javapro.valid.ExpenseValidator;

public class ExpenseValidatorTest {
	private static ExpenseValidator expenseValidator  ; 
	
	 @BeforeAll
	 static void initAll()  {
	        expenseValidator = new ExpenseValidator();
	    	expenseValidator.validate();
	    }
	   
    @Test
    @DisplayName("Test expense validator with invalid expense")
    void testWithInvalidExpense() {
    	List<Expense> invalideExpense = expenseValidator.getInvalidExpenses();
    	invalideExpense.add(new Expense());
    	assertTrue(invalideExpense.isEmpty() == false);
    }

    @Test
    @DisplayName("Test expense validator with valid expense")
    void testWithValidExpense() {
    	List<Expense> valideExpense = expenseValidator.getValidExpenses();
    	valideExpense.add(new Expense());
    	assertTrue(valideExpense.isEmpty() == false);
    }

    @Test
    @DisplayName("Retrieve expenses from a remote location")
    void testRetrieveExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Create expenses from a CSV data")
    void testCreateExpensesFromCsv() {
    	int fileContentData =  expenseValidator.getValidExpenses().size();
    	fileContentData+= expenseValidator.getInvalidExpenses().size();
        assertTrue(fileContentData!=0);   
        }

    @Test
    @DisplayName("Validate expense's employee exists")
    void testIsEmployeeExists() {
    	List<Employe> employes = expenseValidator.getEmployees() ; 
        String name = employes.get(0).getNom();
        assertTrue(ValidationRegle.isEmployeeExists(name)) ;      
        assertTrue(ValidationRegle.isExpenseEmployeValidOrNot(name,expenseValidator.getValidExpenses()));
    }

    @Test
    @DisplayName("Validate if an expense have a legal amount")
    void testHaveLegalAmount() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate if an expense respect enterprise policy")
    void testHavePolicyAmount() {
    	assertTrue(ValidationRegle.validExpenseMonth("newton","12/11/2018",expenseValidator.getValidExpenses()));
    }

    @Test
    @DisplayName("Validate if employees expenses respect the month policy")
    void testRespectMonthPolicy() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Append valid expenses to expenses history")
    void testSaveValidExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Append invalid expenses to expenses history")
    void testSaveInvalidExpenses() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }
}
