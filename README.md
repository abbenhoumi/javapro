# Td1 : Expenses validation

Voici l'application utilisée par la société JeanMich Corporation pour valider les notes de frais de ses employés. 
Cette application a été conçue de manière très chaotique. 
Aucun design n'a été fait et le développeur ne s'est pas soucié du devenir de l'application, la fin de sa mission approchant il ne lui a pas semblé utile de penser à l'avenir de sa "création".
Malgré le fait qu'elle ressemble plus à un tableau de Picasso qu'à la Tour Eiffel, l'application a permis à l'entreprise de répondre à un dispositif légal imposé par l'URSSAF.  

L'entreprise a en effet l'obligation légale de refuser les notes de frais jugées "irraisonnables". L'application ExpenseValidator lui permet depuis 3ans d'extraire les notes de frais invalides.  

Malheureusement pour JeanMich Corporation, un inspecteur URSSAF est venu dans ses locaux pour auditer le système de validation de note de frais et il s'est aperçu d'une erreur sur la validation. 
En plus d'une grosse amende mettant l'entreprise en grande difficulté, l'inspecteur a exigé la correction de l'application ExpenseValidator afin de revenir sur le chemin de la légalité.

C'est donc là que vous intervenez. 

## 0 - Infos pratiques
Pour récupérer les sources de l'application, "forkez" le repo à l'adresse suivante dans votre namespace personnel : 
```
https://gitlab.isima.fr/javapro/expensevalidator
```

Les données utilisée par l'application se trouvent à cette adresse : 
```
https://raw.githubusercontent.com/anthoRx/td1/master/expenses.csv
```

## 1 - Extraire les regles métier 
La première chose à faire lorsque l'on se trouve dans ce cas de figure est d'extraire les règles métier.
Pour vous simplifiez la tâche et parce que ce n'est pas l'objet de ce TP, voici les règles métier d'ExpenseValidator :
- Le montant d'une note de frais de peux être négatif
- La somme des note de frais d'un employé de peux dépasser 1000€ par mois. Au delà toutes ses notes de frais sont considérées comme invalides.
- Les dépenses d'une note de frais ne doivent pas dépasser les plafonds suivants : hotel 180.0€, repas 35.0€, train 200.0€, avion 800.0€, boisson 100.0€


## 2 - Repensez la structure de l'application 
Une analyse rapide du code vous permet de vous apercevoir de l'absence de design, de structure de l'application. 
Cela pose un premier problème puisque vous êtes dans l'incapacité de tester ExpenseValidator de manière automatique. 
Des tests unitaires ont bien été rédigés, malheureusement ExpenseValidator est intestable en l'état et la visite de l'inspecteur a montré que c'est un problème mettant en jeux la survie de l'entreprise.  

Afin de pallier à ce problème, analysez le code existant afin de le rendre plus modulaire et donc plus testable. 
Le SRP "Single Responsability Principle" est ici la clé pour repenser la structure de l'application. 


## 3 - Adaptez les tests
Bien, l'application étant plus modulaire donc plus facilement testable et pouvant s'adapter aux évolutions future, il nous faut encore trouver l'erreur ayant amené le redressement de l'URSSAF.  

Pour cela, adaptez les tests à votre application (car oui désormais c'est la vôtre) afin de découvrir le bug. Dans la foulée, corrigez le assurez-vous que tous les tests sont au vert !    

